package Week03;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        boolean done = false;
        //wrap code in a do while loop so it will keep checking numbers
        do {
            Scanner in = new Scanner(System.in);
            //ask user for numbers
            System.out.println("Enter two numbers to be divided.");
            System.out.print("First number: ");
            //takes input and puts it into a variable
            int x = in.nextInt();
            System.out.print("Input the second number: ");
            int y = in.nextInt();

            //catch data validation issues
            //try {
            //    x = Integer.parseInt(null);
            //    y = Integer.parseInt(null);
            //    done = true;
            //}
            //catch (NumberFormatException ex) {
            //    System.err.println("Invalid number, try again.");
            //}

            //catch exceptions with a try and catch
            try {
                int answer = (x/y);
                System.err.println("Answer: " + answer);
                done = true;
            }
            catch (ArithmeticException why) {
                System.out.println("Divisor is zero, try again.");
            }

        }while (!done);
        System.out.println("Good job, thank you for participating.");
    }
}
